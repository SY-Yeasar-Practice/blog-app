const jwt  = require('jsonwebtoken')
require('dotenv').config()
const User = require('../src/Model/User/model')

const authenticationMiddleware = async (req, res, next) => {
    try {
        const token = req.header('Authorization') //get the token from headers
        //get the dot env file data
        const securityCode = process.env.JWT_SECRET_CODE //ge the security code from dot env
        if(!token) {
            req.isAuth  = false;
            next ()
        }else {
            const isValidToken = await jwt.verify(token, securityCode) //check that is it a valid token or not
            if(isValidToken) {
                const {id, email} = isValidToken //store the token data as a verified userType
                const user = await User.findOne ({
                    _id: id,
                    "personalInfo.email": email
                }).select ("-personalInfo.password")
                if (user) {
                    req.isAuth  = true
                    req.user = user //store the user token data into req as a user
                    next() //sent it as a success in to the controller
                }else {
                    res.isAuth = false;
                    next ()
                }
                
            }else {
                req.isAuth = false
                next ()
            }
        }
    }catch(err) {
        req.isAuth = false
        next ()
        console.log(err)
    }
}

module.exports = authenticationMiddleware

