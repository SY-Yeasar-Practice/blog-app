const {createNewCommentController,
    updateCommentController,
    getAllCommentByBlogId,
    deleteCommentTemporaryController} = require('./controller')
const resolver = {
    createComment: createNewCommentController,
    updateComment: updateCommentController,
    comments: getAllCommentByBlogId,
    deleteComment: deleteCommentTemporaryController
}

module.exports = resolver