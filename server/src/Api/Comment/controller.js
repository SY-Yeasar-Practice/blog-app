const Comment = require('../../Model/Comment/model')
const commentIdGenerator = require('../../../utils/idGenerator')
//create a  new comment 
const createNewCommentController = async ({input}, req) => {
    try {
        // console.log(input)
        const {isAuth} = req //get the isAuth  
        if (isAuth) { //check the user is authenticated or not 
            const  {parent:parentComment, content, blog} = input //get the body input
            const {_id} = req.user
            const commentId = commentIdGenerator("CMNT")
            const struct = {
                blog,
                content,
                user: _id,
                commentId
            }
            // console.log(parentComment)
            if (parentComment) {
                const findParent = await Comment.findOne ({"commentId":parentComment })
                const {commentId} = findParent
                struct.parent = parentComment
                struct.inherit = commentId
            }
            const createNewComment = new Comment (struct) //create a instance of comment
            const saveComment = await createNewComment.save() //save the new comment into the database
            if (saveComment) { //if the message is create then it will execute 
                return {
                    message: "New Comment Published"
                }
            }else {
                return {
                    message: "New Comment Creation Failed"
                }
            }
        }else {
            return {
                message: "Unauthorize user"
            }
        }
    }catch (err) {
        console.log(err)
        return {
            message: err
        }
    }
}

//update comment by id 
const updateCommentController = async ({id, input}, req) => {
    try {
        const {isAuth} = req 
        if (isAuth) { //check that is it a authenticated user or not
            const updateComment = await Comment.updateOne (
                {
                    _id: id
                }, //query
                {
                    "content": input
                }, //update  
                {multi:true} //option 
            )
            if (updateComment.modifiedCount != 0) {
                return {
                    message: "Comment updated"
                }
            }else {
                return {
                    message: "Comment Update Failed"
                }
            }
        }else {
            return {
                message: "Unauthorized user"
            }
        }
    }catch (err) {
        console.log(err)
        return {
            message: err.message
        }
    }
}

//get all comment with reply  by blog id 
const getAllCommentByBlogId = async ({id}, req) => {
    try {
        if (true) { //if the user is authenticated then it will execute
            const getAllComment = await Comment.find (
                {
                    "blog": id,
                    isDelete: false
                }
            ).populate({
                path: "user",
                select: `
                    personalInfo.firstName 
                    personalInfo.lastName 
                    personalInfo.profileImage 
                    personalInfo.email 
                    -_id
                    
                `
            }).populate ({
                path: "blog",
                select: "post title"
            }).select (`
                commentId
                content
                inherit
                isDelete
                _id
            `)
            if (getAllComment.length != 0) { //if comment found then it will happen
                const allComment = []
                getAllComment.map (item => {
                    const jsonItem = JSON.stringify (item)
                    const parseItem = JSON.parse (jsonItem)
                    allComment.push (parseItem)
                })
                //but it filtered same things but not working
                const parent = allComment.filter (data => !data.inherit) //get all root parent comment
                function recursive (p, all) {
                    p.map (singleParent => {
                        singleParent.child = []
                        all.map (singleChild => {
                            if (singleChild.inherit == singleParent.commentId) {
                                singleParent.child = [...singleParent.child, singleChild]
                            }
                        })
                        if (singleParent.child.length) {
                            recursive(singleParent.child, allComment)
                        }else {
                            singleParent.child = []
                        }
                         
                    })
                }
                
                recursive(parent,allComment)
                return {
                    message: "Comment Found",
                    data: parent
                }         
            }else {
                return {
                    message: "No Comment Found"
                }
            }
        }
    }catch (err) {
        return {
            message: err.message
        }
        console.log(err)
    }
}

//delete a comment by id 
const deleteCommentTemporaryController = async ({id}, req) => {
    try {
        const {isAuth} = req //get the auth status
        if (isAuth) {
            const commentUniqueId = id 
            const deleteComment = await Comment.updateOne ( //delete comment temporary by edit the isDelete field
                {
                    _id: commentUniqueId,
                    isDelete: false
                }, //query
                {
                    $set: {
                        isDelete: true
                    }
                }, //update
                {multi:true} //option
            )
            if (deleteComment.modifiedCount != 0) { //if the comment is deleted successfully then it will happen
                return {
                    message: "Comment Delete Successfully"
                }
            }else {
                return {
                    message: "Comment delete failed"
                }
            }
        }else {
            return {
                message: "Unauthorized user"
            }
        }
    }catch(err) {
        console.log(err)
        return {
            message: err.message
        }
    }
}

//export part 
module.exports = {
    createNewCommentController,
    updateCommentController,
    getAllCommentByBlogId,
    deleteCommentTemporaryController
}


