const {buildSchema} = require("graphql")

const comment = `
    type personalInfo {
        firstName : String
        lastName: String
        profileImage: String
        email: String
    }
    type officialInfo {
        blog: [Blog]
    }
    type User {
        _id: String
        personalInfo: personalInfo
        officialInfo: officialInfo
    }
    type Blog {
        _id: String
        user: User
        post: String
        category : [String]
        keyWord: [String]
        title: String
    }
    type Comment {
        _id: String
        content: String!
        user: User
        commentId: String
        inherit: String
        child: [Comment]
        blog: Blog
    }
`
const commentInput = `
    input commentData {
        parent: String
        content: String
        blog: String
    }
`
const commentSchema = buildSchema (`
    ${comment}
    ${commentInput}
    type response {
        message: String!
        data: [Comment]
    }
    type Query {
        comments (id: ID!) : response
    }
    type Mutation {
        createComment (input: commentData): response
        updateComment (input: String id: ID!) : response
        deleteComment (id: ID!) : response
    }
`)

module.exports = commentSchema