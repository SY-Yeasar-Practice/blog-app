const Article = require('../../Model/Article/model')

const createArticleController = async ({input}) => {
    try {
        const createNewArticle = new Article (input)
        const saveNewArticle = await createNewArticle.save () //save new Article 
        if (saveNewArticle) {
            return {
                message: "New Article created successfully",
                data: saveNewArticle
            }
        }else {
            return {
                message: "New Article creation failed"
            }
        }
    }catch (err) {
        console.log(err)
    }
}

const updateArticleController = async ({id, input}) => {
    if (id) {
        const updateArticle = await Article.updateOne (
            {
                _id: id
            }, //query
            {
                $set: input
            }, //update
            {multi: true}, //option
        )
        if (updateArticle.nModified != 0) {
            const getArticle = await Article.findOne ({
                _id:id
            })
            if (getArticle._id) {
                return {
                    message: "Article Updated",
                    data: getArticle
                }
            }else {
                return {
                    message: "Article Not found"
                }
            }
        } else {
            return {
                message: "Update failed"
            }
        }
    }else {
        return {
            message: "ID required" 
        }
    }
}

const deleteArticleController = async ({id}) => {
    if (id) {
        const deleteArticle = await Article.deleteOne ({
            _id: id
        })
        if (deleteArticle) {
            return {
                message: "Article has been deleted"

            }
        }else {
            return {
                message: "Article Delete failed"
            }
        }
    }else {
        return {
            message: "Id Required"
        }
    }
}

const getAllArticle = async () => {
    const findAll = await  Article.find ()
    return findAll
}

const articleById = ({id, a}) => {
    console.log(id, a);
    const articles = getAllArticle()
    const article = articles.find(art => art.id == id)
    return article; 
}
module.exports = {
    getAllArticle,
    articleById,
    createArticleController,
    updateArticleController,
    deleteArticleController
}