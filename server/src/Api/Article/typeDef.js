const {buildSchema} = require("graphql")


const schema = buildSchema (`
    type Article {
        id: ID!
        title: String!
        content : String !
    }
    input ArticleInput {
        title: String!
        content : String !
    }
    input ArticleUpdateInput {
        title: String
        content: String
    }
    type CreateResponse {
        message: String!,
        data: Article
    }
    type Query {
        articles : [Article]
        article (id: ID!) : Article
    }
    type Mutation {
        createArticle (input: ArticleInput): CreateResponse,
        updateArticle (id: ID! input: ArticleUpdateInput) : CreateResponse,
        deleteArticle (id: ID!) : CreateResponse 
    }
`)

module.exports = schema ;