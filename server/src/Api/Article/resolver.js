const {getAllArticle, 
        articleById, 
        createArticleController, 
        updateArticleController,
        deleteArticleController} = require('./controller')

const resolver = {
    articles : getAllArticle,
    article: articleById,
    createArticle: createArticleController,
    updateArticle: updateArticleController,
    deleteArticle: deleteArticleController
}

module.exports = resolver