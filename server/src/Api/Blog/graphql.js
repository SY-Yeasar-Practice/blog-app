const {graphqlHTTP} = require("express-graphql")
const resolver = require("./resolver")
const schema = require("./typeDef")

const httpReq = graphqlHTTP ({
    schema,
    rootValue: resolver,
    graphiql: true
})

module.exports = httpReq