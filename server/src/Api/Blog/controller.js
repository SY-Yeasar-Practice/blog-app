const Blog = require('../../Model/Blog/model')
const {categoryFilter, timeDurationFilter, sortQuery, searchQuery} = require("../../query/Blog/query")
const User = require ("../../Model/User/model")
//create a new blog part 
const createNewBlogController = async ({input}, req) => {
    try {
        const {isAuth} = req //get the auth part
        if (isAuth) { //check if it is authenticated or not 
            const {_id} = req.user //logged in user
            const {post, category, keyWord, title} = input
            const createBlog = new Blog({
                "user": _id,
                post,
                category,
                keyWord,
                title
            }) //create a new instance of Blog
            const saveBlog = await createBlog.save() //save new blog 
            if (saveBlog) {
                const {_id:blogId} =   saveBlog
                const updateUser = await User.updateOne ( //update the user and push the blog 
                    {
                        _id
                    }, //query
                    {
                        $push: {
                            "officialInfo.blog": blogId
                        }
                    }, //update
                    {multi:true}, //option
                )
                if (updateUser.modifiedCount != 0) {
                    return {
                        message: 'New Blog Created successfully',
                        blog: saveBlog
                    }
                }else {
                    return {
                        message: 'New Blog Created successfully but user profile is not updated',
                        blog: saveBlog
                    }
                }
                
            }else {
                return {
                    message: "Blog Creation Failed"
                }
            }
        }else {
            return {
                message: "Unauthorized user"
            }
        }
    }catch (err) {
        return {
            message: err.message
        }
        console.log(err)
    }
}

//user can only see own blog 
const seeOwnBlogController = async ({},req) => {
    try {
        const {isAuth} = req //get isAuth status 
        if (isAuth) { //if the user is  authenticated  then it will execute
            const {_id} = req.user //get the logged in user data
            const findBlog = await Blog.find (
                {
                    "user": _id
                }
            ).populate ({
                path: "user",
                select: `
                    -personalInfo.password
                `
            })
            if (findBlog) {
                return {
                    message: `${findBlog.length} blog found`,
                    blogs: findBlog
                }
            }else {
                return {
                    message: "Blog not found"
                }
            }
        }else {
            return {
                message: "Unauthorized user"
            }
        }
    }catch (err) {
        console.log(err)
        return {
            message: err.message
        }
    }
}

//get individual blog by id  
const getIndividualBlogController = async ({id}, req) => {
    try {
        if (true) { 
            const findOneBlog = await Blog.findOne ({_id: id, "isDelete": false}).populate ({
                path: "user",
                select: `
                    -personalInfo.password
                `
            })
            if (findOneBlog) { //if the blog has found then it will  executed
                const {category, keyWord, _id} = findOneBlog
                const queryStructure = { //create the query structure
                    $and: [
                        {
                            "_id": {
                                $ne: _id
                            }
                        },
                        {
                            $or: []
                        },
                        {
                            "isDelete": false
                        }
                    ]
                }
                if (category && (category.length != 0)) { //query by category and create the structure
                    const query = {
                        "category": {
                            $in: category
                        }
                    }
                    queryStructure.$and[1].$or.push(query)
                }
                if (keyWord && (keyWord.length != 0)) { //query by keyword and create the structure
                    const query = {
                        "keyWord": {
                            $in: keyWord
                        }
                    }
                    queryStructure.$and[1].$or.push(query)
                }
                const findBlogs = await Blog.find (queryStructure).sort ({ //find all related blog of following id's blog
                    "createdAt": -1
                }).populate ({
                    path: "user",
                    select: `-personalInfo.password`
                })
                if (findBlogs.length) { //if related data found then it will happen
                    return {
                        message: "Blog Found!!",
                        blog: findOneBlog,
                        relatedBlogs: findBlogs
                    }
                } else {
                    return {
                        message: "Blog Found!!",
                        blog: findOneBlog,
                        relatedBlogs: []
                    }
                }
            }else {
                return {
                    message: "Blog Not Found"
                }
            }
        }
    }catch (err) {
        console.log(err)
        return {
            message: err.message
        }
    }
}

//update blog by id
const updateBlogByIdController = async ({id, input}, req) => {
    try {
        const {isAuth} = req 
        if (isAuth) { //check if the user is  authenticated  then it will
            const updateBlog = await Blog.updateOne ( //update the blog query by id 
                {
                    _id: id
                }, //query
                {
                    $set: input
                }, //update 
                {multi: true} //option
            )
            if (updateBlog.modifiedCount != 0) {
                const updatedBlog = await Blog.findOne ({_id:id})
                if (updatedBlog) { //if find the updated blog successfully
                    return {
                        message: "Blog Updated successfully",
                        blog: updatedBlog
                    }
                }else {
                    return {
                        message: "Blog already updated"
                    }
                }
            }else {
                return {
                    message: "Blog Update Failed"
                }
            }
        }else {
            return {
                message: "Unauthorized user "
            }
        }
    }catch (err) {
        console.log(err)
        return {
            message: err.message
        }
    }
}

//get all blog by filter 
const getALlBLogController = async ({input}, req) => {
    const {query, page, sortBy, search} = input //get the query input from body 
    // const {isAuth} = req 
    if (true) { //check the authentication of user
        const {category, duration} = query
        const queryStruct = {
            $and: [
                {
                    "isDelete": false
                }
            ]
        } //create main structure for database query in dynamic way
        const sortStruct = {}
        //category will be and array of string
        if (category != undefined && category.length != 0) categoryFilter(category, queryStruct) //if it is filtered by category then it will execute
        //duration will  latest || thismonth || lastsevendays
        if (duration) timeDurationFilter(duration, queryStruct)//if there have any duration filter then it will happen
        
        //sort by will be latest || A-Z || Z-A
        if (sortBy) sortQuery(sortBy,sortStruct ) //if there have any sort query then it will happen
        
        if (search) searchQuery(search, queryStruct) //check the search input and will create the structure of query for search
        
        if (!queryStruct.$and.length) delete  queryStruct.$and //if query field is empty then it will happen
        const findAllData = await Blog.find (queryStruct).populate({ 
            path: "user",
            select: `personalInfo.firstName personalInfo.lastName personalInfo.profileImage`
        }).sort (sortBy)
       
        if (findAllData.length != 0 ) { //if blog found then it will happen
            const totalBlog = findAllData.length //get the number of total pages
            const limit = 9 //set the limit of data
            const totalPage = Math.ceil(totalBlog / limit) //total number of page need 
            const skip = ((page || 1) * limit) - limit //logic of skip data
            const findOriginalData = await Blog.find (queryStruct).populate({
                path: "user",
                select: `personalInfo.firstName personalInfo.lastName personalInfo.profileImage`
            }).sort (sortBy).limit (limit).skip (skip) //query all blog data with pagination
            if (findOriginalData.length != 0) { //if data found then it will happen
                return {
                    message: "Blog Found",
                    blogs: findOriginalData,
                    totalPage,
                }
            }else {
                return {
                    message: "No Blog Found"
                }
            }
        }else {
            return {
                message: "No Blog Found"
            }
        }
    }
}

//delete blog by id 
const deleteBlogByIdController = async ({id}, req) => {
    try {
        const {isAuth} = req
        if (isAuth) { //check that if is it authentic user or not
            const deleteBlog = await Blog.updateOne ( //delete the blog
                {
                    $and : [
                        {
                            _id: id
                        },
                        {
                            "isDelete": false
                        }
                    ]
                }, //query
                {
                    "isDelete": true
                }, //update
                {multi: true} //option
            )   
           
            if (deleteBlog.modifiedCount != 0) { //if blog delete successfully done then it will execute
                return {
                    message: "Blog has been deleted successfully"
                }
            }else {
                return {
                    message: "Blog Delete failed"
                }
            }
            
        }else {
            return {
                message: "Unauthorized User"
            }
        }
    }catch (err) {
        console.log(err)
        return {
            message: err
        }
    }
}

module.exports = {
    createNewBlogController,
    seeOwnBlogController,
    getIndividualBlogController,
    updateBlogByIdController,
    getALlBLogController,
    deleteBlogByIdController
}

