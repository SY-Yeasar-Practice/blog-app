const {createNewBlogController,
    seeOwnBlogController,
    getIndividualBlogController,
    updateBlogByIdController,
    getALlBLogController,
    deleteBlogByIdController} = require('./controller')

const resolver = {
    createNewBlog : createNewBlogController,
    showOwnBlog : seeOwnBlogController,
    showOneBlog: getIndividualBlogController,
    editBLog: updateBlogByIdController,
    getAllBlog: getALlBLogController,
    deleteBlog: deleteBlogByIdController
}

module.exports = resolver