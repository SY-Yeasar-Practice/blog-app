const {buildSchema} = require('graphql')
const blog = `
     type personalInfo {
        firstName : String
        lastName: String
        profileImage: String
        email: String
    }
    type officialInfo {
        blog: [Blog]
    }
    type User {
        id: ID!
        personalInfo: personalInfo
        officialInfo: officialInfo
    }
    type Blog {
        id: ID!
        user: User
        post: String
        title: String
        category : [String]
        keyWord: [String]
    }
`
const inputField = `
    input blogInput {
        post: String!
        category: [String]!
        keyWord: [String]!
        title: String!
    }
    input blogEditInput {
        post: String
        category: [String]
        keyWord: [String]
        title: String
    }
    input getALlQuery {
        category: [String]
        duration: String
    }
    input getAllBlogInput {
        query: getALlQuery
        page: Int
        sortBy: String
        search: String
    }
`
const blogSchema =  buildSchema (`
    ${blog}
    ${inputField}
    type response {
        message: String!
        blog: Blog
        blogs: [Blog]
        relatedBlogs: [Blog]
    }
    type responseAllBlog {
        message: String!
        blogs : [Blog]
        totalPage: Int
    }
    
    type Mutation {
        createNewBlog (input: blogInput) : response
        editBLog (id: ID! input: blogEditInput): response
        getAllBlog (input: getAllBlogInput) : responseAllBlog
        deleteBlog (id: ID!) : response
    }
    type Query {
        showOwnBlog : response
        showOneBlog (id: ID!) : response
    }
`)

module.exports = blogSchema
