const {createNewUserController,
    loginController,
    uploadProfilePictureController,
    profileUpdateController} = require ("./controller")


const resolver = {
    createUser: createNewUserController,
    login: loginController,
    uploadProfilePicture: uploadProfilePictureController,
    updateUser: profileUpdateController
}

module.exports = resolver