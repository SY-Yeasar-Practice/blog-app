const {buildSchema} = require ('graphql')

let user = `
    type personalInfo {
        firstName : String
        lastName: String
        profileImage: String
        email: String
    }
    type Blog {
        id: ID
        user: User
        post: String
        category : [String]
        keyWord: [String]
    }
    type officialInfo {
        blog: [Blog]
    }
    type User {
        id: ID
        personalInfo: personalInfo
        officialInfo: officialInfo
    }
`
const inputData = `
    input userInput  {
        firstName: String!
        lastName: String !
        profileImage: String
        email: String!
        password: String!
    }
    input loginInput {
        email: String !
        password: String !
    }
    input profilePicture {
        base64: String!
        size: Int!
    }
    input updateProfile {
        firstName: String
        lastName: String 
        email: String
    }
`
const userSchema = buildSchema (`
    ${user}
    ${inputData}
    type userResponse {
        message: String!
        data: User
        token: String
    }
    type Query {
        users : [User]!
    }
    type Mutation {
        createUser (dataInput:userInput): userResponse
        login (input: loginInput): userResponse
        uploadProfilePicture (input: profilePicture): userResponse
        updateUser (input: updateProfile ): userResponse
    }
`)
module.exports = userSchema