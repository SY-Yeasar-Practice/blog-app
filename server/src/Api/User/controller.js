const User = require("../../Model/User/model")
const bcrypt = require ("bcrypt")
const jwt = require("jsonwebtoken")
const {singleFileUploader:fileUploader} = require("../../../utils/singleFileUploader")
const fs = require("fs")

//dot env file data 
const jwtSecretCode = process.env.JWT_SECRET_CODE //get the jwt secret code

//create a new user
const createNewUserController = async ({dataInput}) => {
    try {
        const {firstName, lastName, email, password} = dataInput //get the input data from body
        const isAvailableEmail = await User.findOne ({
            "personalInfo.email": email
        })
        if (!isAvailableEmail) {
            const hashedPassword = await bcrypt.hash (password, 10)
            if (hashedPassword) {
                const body = {
                        personalInfo: {
                            firstName,
                            lastName,
                            email,
                            password: hashedPassword
                        }
                    } //set the data to store into database according to schema
                    const createNewUser = await new User (body) //create a instance of user 
                    const saveNewUser = createNewUser.save() //save new user 
                    if (saveNewUser) {
                        return {
                            message: "User Created Successfully",
                            data: saveNewUser
                        }
                    }else {
                        return {
                            message: "User Creation Failed",
                        }
                    }
                }else {
                    return {
                            message: "Password Hashing Problem"
                    }
                }
            }else {
                return {
                    message: "This Email is already in use. Please Try with another email address"
                }
            }
    }catch (err) {
        return {
            message: err.message 
        }
    }
}

//login controller 
const loginController = async ({input}, req) => {
    try {
        const {email, password} = input //get the email and password from input body
        const isAvailableUser = await User.findOne ({
            "personalInfo.email": email
        })
        if (Object.keys(isAvailableUser).length) { //check that is user is available or not
            const {password:dataBasePassword} = isAvailableUser.personalInfo
            const isMatch = await bcrypt.compare (password, dataBasePassword) //check that password 
            if  (isMatch) {
                const {personalInfo: {email}, _id:id} = isAvailableUser
                const data = {
                    id,
                    email
                }
                const token = await jwt.sign (data, jwtSecretCode)
                if (token) {
                   return {
                       message: "Login successful",
                       token,
                       data: isAvailableUser
                   } 
                }else {
                    return {
                        message: "Token Invalid",
                    }
                }
                
            }else {
                return {
                    message: "Password incorrect"
                }
            }
        }else {
            return {
                message: "User not Found"
            }
        }
    }catch (err) {
        return {
            message: err.message
        }
    }
}

//update profile picture 
const uploadProfilePictureController = async ({input}, req) => {
    try {
        const {isAuth} = req 
        const {base64, size} = input //get the input data from body
        const validExtensions = ["jpeg", "jpg", "png"]
        const file = {
            base64,
            size
        } //set the sent file format for upload
        if (isAuth) {
            const {_id,user:{personalInfo:{profileImage}}} = req //get the logged in user
            //upload the image first 
            const {fileUrl, fileAddStatus, extensionValidation} = fileUploader (file, validExtensions) //upload the image here
            if (extensionValidation) { //if the extension validation successfully done 
                if (fileAddStatus) { //if the file is successfully uploaded then it will execute
                     if (profileImage) { //if profile image is exist then it will happen
                        const existProfileImage = profileImage.split("/")[1]
                        //delete exist file from server 
                        fs.unlink (`${__dirname}/../../public/${existProfileImage}`, async (err) => {
                            if (err) {
                                return {
                                    message: err.message
                                }
                            }else {
                                const updateProfilePicture = await User.updateOne (
                                    {
                                        _id
                                    }, //query
                                    {
                                        $set : {
                                            "personalInfo.profileImage": fileUrl
                                        }
                                    }, //update
                                    {multi: true} //option
                                )
                                if (updateProfilePicture.nModified != 0) {
                                    return {
                                        message: "Profile Successfully updated"
                                    }
                                }else {
                                    return {
                                        message: "File store failed"
                                    }
                                }
                            }
                        }) //delete the exist file from server
                    }else {
                       const uploadProfilePicture = await User.updateOne (
                            {
                                _id
                            }, //query
                            {
                                $set: {
                                    "personalInfo.profileImage": fileUrl
                                }
                            }, //update 
                            {multi: true} //option
                        ) 
                        if (uploadProfilePicture.nModified != 0 ) { //if file uploaded successfully done then it will happen
                            return {
                                message: "File uploaded successfully"
                            }
                        } else {
                            return {
                                message: "Image Upload Failed"
                            }
                        }
                    }
                }else {
                    return {
                        message: "File Upload Failed"
                    }
                }
            }else {
                return {
                    message: `Only ${validExtensions.join(",")} are accepted`
                }
            }
           
        }else {
            return {
                message: "Unauthorized user "
            }
        }
    }catch (err) {
        return {
            message: err.message 
        }
    }
}

//update Profile controller 
const profileUpdateController = async ({input}, req) => {
    try {
        const {isAuth} = req //get authentication part
        if (isAuth) { //check that is user is authenticate or not
            const {_id} = req.user //get the id from logged in user 
            const {firstName, lastName, email} = input
            let data = {} //this will create the structure of update data 
            if (firstName) { //check is firstName exist or not
                data = {
                    ...data,
                    "personalInfo.firstName" : firstName
                }
            }
            if (lastName) data = {
                ...data,
                "personalInfo.lastName" : lastName
            } //check is lastName  exist or not

            if (email) { //check if email Exist  or not 
                //check is email is exist or not 
                const isExistMail = await User.findOne ({"personalInfo.email" : email}) 
                console.log(isExistMail)
                if (!isExistMail) {
                    data = {
                        ...data,
                        "personalInfo.email" : email
                    }
                }else {
                    return {
                        message: "Email already exists Try with another email address"
                    }
                }
            }
            // console.log(data)

            const updateProfile = await User.updateOne ( //update the exist user data
                {
                    _id
                }, //query
                {
                    $set : {
                        ...data
                    }
                }, //update 
                {multi: true} //option
            )
            if (updateProfile.nModified != 0 ) {
                const findUpdateUser = await User.findOne ({
                    _id
                })
                if (findUpdateUser) {
                     return {
                        message: "Profile Has been updated",
                        data: findUpdateUser
                    }
                }else {
                    return {
                        message: "User Not found"
                    }
                }
               
            }else {
                return {
                    message: "User Update Failed"
                }
            }
        }else {
            return {
                message: "Unauthorized User"
            }
        }
    }catch (err) {
        console.log(err)
        return {
            message: err.message
        }
    }
}

//

//export part 
module.exports = {
    createNewUserController,
    loginController,
    uploadProfilePictureController,
    profileUpdateController
}
