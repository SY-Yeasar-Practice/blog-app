const mongoose = require('mongoose');
const Schema = mongoose.Schema; 

const commentSchema = new Schema ({
    blog: {
        type: Schema.Types.ObjectId,
        ref: "Blog"
    },
    commentId: {
        type: String,
        required: true
    },
    inherit: {
        type: String,
        default: ""
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    content: {
        type: String
    },
    isDelete: {
        type: Boolean,
        default: false
    }

},{
    timestamps: true
})

module.exports = mongoose.model ("Comment", commentSchema)