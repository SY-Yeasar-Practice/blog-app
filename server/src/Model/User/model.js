const mongoose = require('mongoose');
const Schema = mongoose.Schema; 

const userSchema = new Schema ({
    personalInfo : {
        firstName: {
            type: String,
            required: true
        },
        lastName : {
            type: String,
            required: true
        },
        profileImage: {
            type: String,
            default: ""
        },
        email: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true,
            min: 6,
            max: 30
        }
    },
    officialInfo: {
        blog: [
            {
                type: Schema.Types.ObjectId,
                ref: "Blog"
            }
        ]
    }
},{
    timestamps: true
})

module.exports = mongoose.model ("User", userSchema)