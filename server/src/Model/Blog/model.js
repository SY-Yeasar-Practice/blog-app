const mongoose = require('mongoose');
const Schema = mongoose.Schema; 

const blogSchema = new Schema ({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    title: {
        type: String
    },
    post: {
        type: String,
        default: ""
    },
    category : [{
        type: String,
        required: true
    }],
    keyWord: [{
        type: String,
        required: true
    }],
    isDelete: {
        type: Boolean,
        default: false
    }
    
}, {
    timestamps: true
})

module.exports = mongoose.model ("Blog", blogSchema)