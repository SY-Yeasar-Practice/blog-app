const express = require('express');
const app = express();
const mongoose = require('mongoose');
require("dotenv").config();
const auth = require('./middleware/auth');
const articleQl = require('./src/Api/Article/graphql')
const userQl = require('./src/Api/User/graphql')
const blogQl = require('./src/Api/Blog/graphql')
const commentQL = require('./src/Api/Comment/graphql')
const graphqlHeader = require('express-graphql-header')
 
//dot env file 
const mongoUrl = process.env.MONGO_URL //get the mongo url from dot env file
const port = process.env.PORT //get the port from dot env file 

//global middleware file
app.use (express.json({limit: "250mb"}))
app.use(express.urlencoded({ extended: true, limit: '250mb'}));
app.use (express.static ("public"))

//create the server 
app.listen (port, () => console.log(`Server is running on ${port}`))

//create the database 
mongoose.connect (mongoUrl , {
    useUnifiedTopology: true,
})
.then (() => console.log(`Database is connect to the server`))
.catch (err => console.log(`${err}`))

app.use (auth)

app.use ("/article",graphqlHeader, articleQl)
app.use ("/user", graphqlHeader,  userQl)
app.use ("/blog", graphqlHeader, blogQl)
app.use ("/comment",graphqlHeader, commentQL)
