import React, {useState} from 'react'
import style from "./style.module.css"

const TreeRecursive = ({data , type}) => {
    const [showReplyField, setShowReplyField ] = useState (false) 
    const [selectItem , setSelectItem ] = useState (null)
    const [replyTo, setReplyTo] = useState ("")
    const replyHandler = (e, content, ind) => {
        e.preventDefault();
        setShowReplyField (!showReplyField)
        setSelectItem (ind)
        setReplyTo (content)
    }
    return (
        <div className = {`${style.main}`}>
            {
                data.map ((item, ind) => {
                    return (
                            <div className={`${style.innerWrap}`}>
                                <div key = {ind} className = {`${type == "child" ? style.comment_reply  : style.comment_reply} ${style.comment}`}>
                                    <h5>{type == "child" ? "Reply" : "Comment"}: {item.content}</h5>
                                    <button onClick={(e) => replyHandler (e, item.content, ind)}>Reply</button>
                                    {/* <span style = {{borderBottom : "2px solid rgba(0, 0, 0, 0.1)"}} ></span> */}
                                    {
                                        item.child.length ? <TreeRecursive data = {item.child} type = {`child`}/>: ""
                                    }
                                    {
                                        showReplyField && selectItem == ind
                                        &&
                                        <textarea id="txtid" name="txtname" rows="1" cols="80" maxlength="200" placeholder= {`Reply to ${replyTo}`}>
                                        </textarea>
                                    }
                                    <br />
                                    
                                </div>
                            </div>
                    )
                })
            }
        </div>
    )
}

export default TreeRecursive
