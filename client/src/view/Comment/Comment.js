import React from 'react'
import TreeRecursive from '../../component/TreeRecursive'
import data from '../../../utils/data'

const Comment = () => {
    return (
        <div>
            <TreeRecursive data = {data} />
        </div>
    )
}

export default Comment
