const data = [
    {
        content: "Comment 1",
        child: [
            {
                content: "Reply 1",
                child: [
                    {
                        content: "Reply 1 - 1",
                        child : []
                    },
                    {
                        content: "Reply 1 - 2",
                        child : []
                    },
                    {
                        content: "Reply 1 - 3",
                        child : []
                    }
                ]
            },
            {
                content: "Reply 2",
                child: [
                    {
                        content: "Reply 2 - 1",
                        child: [
                            {
                                content: "Reply 2 - 1 - 1 ",
                                child: []
                            },
                            {
                                content: "Reply 2 - 1 - 2 ",
                                child: []
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        content: "Comment 2",
        child: [
            {
                content: "Reply - 1",
                child : []
            },
            {
                content: "Reply - 2",
                child: [
                    {
                        content: "Reply - 2 - 1",
                        child: []
                    },
                    {
                        content: "Reply - 2 - 2",
                        child: []
                    }
                ]
            }
        ]
    }
]

export default data